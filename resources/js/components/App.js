import React from 'react';
import ReactDOM from 'react-dom';
import Top from './Top';
import Left from './Left';
import Right from './Right';
import First from './First';
import Second from './Second';
import Third from './Third';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      menus: [{
        id: 1,
        name: 'first'
      },{
        id: 2,
        name: 'second'
      },{
        id: 3,
        name: 'third'
      }],
      current: 1,
      timeStamp: 0,
    };
    this.handlerMenuClick = this.handlerMenuClick.bind(this);
    setTimeout(this.addTimeStamp, 5000);
  } 
  
  handlerMenuClick(current) {
    this.setState({
      current
    });
  }
  
  addTimeStamp = () => {
    console.log('addTimeStamp()');
    this.setState({timeStamp: this.state.timeStamp + 1});
    setTimeout(this.addTimeStamp, 5000);
  };
  
  render() {
    const {menus, current, timeStamp} = this.state;
    const Content = current === 1 ? First : current === 2 ? Second : Third;
    return (
      <div>
        <Top 
          title='React Workshop'
          />
        <div className='content'>
          <Left 
            menus={menus}
            current={current}
            handlerMenuClick={this.handlerMenuClick}
          />
          <Right>
            <Content
              timeStamp={timeStamp}
              />
          </Right>
        </div>
       </div>
    );
  }
}

export default App;

if (document.getElementById('app')) {
  ReactDOM.render(<App />, document.getElementById('app'));
}