import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';

class Left extends React.Component {
  static propTypes = {
    menus: PropTypes.array,
    current: PropTypes.number,
    handlerMenuClick: PropTypes.func,
  }

  static defaultProps = {
    menus: [],
    current: 1,
    handlerMenuClick: () => console.warn('under construct!'),
  }
  
  render() {
    const {menus, current, handlerMenuClick} = this.props;
    const menu = _.map(menus, menu => (
      <li 
        key={menu.id}
        className={menu.id === current ? 'active': ''}
        onClick={() => handlerMenuClick(menu.id)}
      >{menu.name}</li>
    ));
    return (
      <div id='left'>
        <ul>{menu}</ul>
      </div>
    );
  }
}

export default Left;
