import React from 'react';
import PropTypes from 'prop-types';

class First extends React.Component {
  static propTypes = {
    timeStamp: PropTypes.number
  }

  static defaultProps = {
    timeStamp: 0
  }
      
  constructor(props) {
    super(props);
    console.log('constructor()');
    console.log('this.props=' + JSON.stringify(this.props));
    this.state = {
      count: 0
    };
    this.handlerButtonClick = this.handlerButtonClick.bind(this);    
  }
  
  handlerButtonClick() {
    this.setState({
      count: this.state.count + 1
    });
  }  

  componentDidMount() {
    console.log('componentDidMount()');
    // Request
  }

  componentWillUnmount() {
    console.log('componentWillUnmount()');
  }
  
  shouldComponentUpdate(nextProps, nextState) {
    let shouldUpdate = true;
    console.log('shouldComponentUpdate()');
    console.log('this.props=' + JSON.stringify(this.props) + ', nextProps=' + JSON.stringify(nextProps));
    console.log('this.state=' + JSON.stringify(this.state) + ', nextState=' + JSON.stringify(nextState));
    if(nextState.count > 2) {
      shouldUpdate = false;
    }
    return shouldUpdate;
  }  
  
  render() {
    console.log('render()');
    console.log('this.props=' + JSON.stringify(this.props));
    console.log('this.state=' + JSON.stringify(this.state));
    return (
      <div className='content'>
        <h1>first</h1>
        <div>
          <span>count = {this.state.count}</span>
          <button 
            onClick={this.handlerButtonClick} >
            add 1
          </button>
        </div>
        <div>timeStamp = {this.props.timeStamp}</div>
      </div>
    );
  }
}

export default First;
