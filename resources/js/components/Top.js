import React from 'react';
import PropTypes from 'prop-types';

class Top extends React.Component {
  static propTypes = {
    title: PropTypes.string,
  }

  static defaultProps = {
    menus: '',
  }

  render() {
    return (
      <div id='top'>
        <h1>{this.props.title}</h1>
      </div>
    );
  }
}

export default Top;
